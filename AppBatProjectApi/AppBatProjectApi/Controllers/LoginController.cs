﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Service.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Pages.Account.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AppBatProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUserService _userService;
        public LoginController(IUserService userService)
        {
            _userService = userService;
        }
         
        //ejemplo de cambio
        // GET: api/LoginController
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/LoginController/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LoginController
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody] Login loginModel)
        {
            try
            {
                User userExist = _userService.Login(loginModel);

                if (userExist == null)
                    return BadRequest("Usuario no existente");

                List<Claim> claims = new List<Claim>();
                claims.Add(new Claim("UserId", userExist.Id.ToString()));
                claims.Add(new Claim("Role", userExist.Role.ToString()));

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("[Clave]"));
                JwtSecurityToken token = new JwtSecurityToken(
                        claims: claims,
                        issuer: "issuer",
                        audience: "audience",
                        expires: DateTime.Now.AddDays(15),
                        signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
                    );

                return new OkObjectResult(new { Token = new JwtSecurityTokenHandler().WriteToken(token) });

            }
            catch (Exception)
            {

                throw;
            }
        }

        // PUT: api/LoginController/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
