﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppBatProjectApi.Service.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppBatProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        //Inyeccion de dependencias (usar metodos de otra clase o interfaz)
        private readonly IPaymentService _paymentService;
        public PaymentController(IPaymentService paymentService)
        {
            this._paymentService = paymentService;
        }
        //Inyeccion de dependencias (usar metodos de otra clase o interfaz)


        // GET: api/Payment
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                return Ok(await _paymentService.GetAllPayment());
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // GET: api/Payment/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Payment
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Payment/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
