﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Service.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace AppBatProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        private readonly INoteService _noteService;
        public NoteController(INoteService noteService)
        {
            this._noteService = noteService;
        }


        // GET: api/Note
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_noteService.GetAllNote());
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }


        // GET: api/Note/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                if (_noteService.GetNote(id).Result == null)
                    return BadRequest();
                return Ok(_noteService.GetNote(id).Result);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // POST: api/Note
        [HttpPost]
        public IActionResult Post(int id, [FromBody] Note note)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            try
            {
                _noteService.InsertNote(note);
                return Ok(200);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: api/Note/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Note note)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            try
            {
                if (id == note.Id)
                    return Ok(_noteService.UpdateNote(note));
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_noteService.DeleteNote(id));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
