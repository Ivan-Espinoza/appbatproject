﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AppBatProjectApi.Service.Service;
using AppBatProjectApi.Model.Model;

namespace AppBatProjectApi.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            this._userService = userService;
        }

        // GET: api/User
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_userService.GetUsers());
            }
            catch (Exception)
            {
                return BadRequest("Error al momento de hacer la peticion");
            }
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                if (_userService.Get(id).Result == null)
                    return BadRequest("Error al momento de hacer la peticion");
                return Ok(_userService.Get(id).Result);
            }
            catch (Exception)
            {
                return BadRequest("Error al momento de hacer la peticion");
            }
        }

        // POST: api/User
        [HttpPost]
                public IActionResult Post([FromBody] User userValue)
        {
            //userValue.Id = 0;
            if (!ModelState.IsValid)
                return BadRequest("Error al momento de hacer la peticion");
            try
            {
                _userService.AddUser(userValue);
                return Ok(200);
            }
            catch (Exception)
            {
                return BadRequest("Error al momento de hacer la peticion");
            }
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]User userValue)
        {
            if (!ModelState.IsValid)
                return BadRequest("Error al momento de hacer la peticion");
            try
            {
                if (id == userValue.Id)
                    return Ok(_userService.UpdateUser(userValue));
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(_userService.Delete(id));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}

