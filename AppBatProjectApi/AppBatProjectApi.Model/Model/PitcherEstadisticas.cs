﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppBatProjectApi.Model.Model
{
    public class PitcherEstadisticas
    {
        public int Id { get; set; }
        public string NamePlayer { get; set; }
        public string Team { get; set; }
        public int NumberGame { get; set; }
        public int GameWiner { get; set; }
        public int GameLoser { get; set; }
        public int Era { get; set; }

    }
}
