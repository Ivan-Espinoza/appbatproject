﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AppBatProjectApi.Model.Model
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
