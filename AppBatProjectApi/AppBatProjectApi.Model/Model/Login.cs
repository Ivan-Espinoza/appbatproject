﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppBatProjectApi.Model.Model
{
    public class Login
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
