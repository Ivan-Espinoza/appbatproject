﻿using AppBatProjectApi.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AppBatProjectApi.Model.Model
{
    public class User:Entity
    {
        [Required, MaxLength(100)]
        public string Name { get; set; }
        public TypeUser Role { get; set; }
        [Required, MaxLength(150)]
        public string LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Range(18, 50)]
        public int? YearsOld { get; set; }
        [MaxLength(10)]
        public string Gender { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Required, Phone]
        public string PhoneNumber { get; set; }

    }
}
