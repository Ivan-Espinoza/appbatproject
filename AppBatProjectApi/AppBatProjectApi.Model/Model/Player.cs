﻿using AppBatProjectApi.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AppBatProjectApi.Model.Model
{
    public class Player :Entity
    {
        [Required,Range(minimum:5,maximum:10)]
        public string Name { get; set; }
        [Required, Range(minimum: 5, maximum: 10)]
        public string FirstLastName { get; set; }
        [Required]
        public int Position { get; set; }
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required,Range(minimum:16,maximum:30)]
        public int YearsOld { get; set; }
        [Required,Range(minimum:15,maximum:100)]
        public string Aptitud { get; set; }

    }
}
