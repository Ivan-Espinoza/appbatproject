﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AppBatProjectApi.Model.Model
{
    public class Team:Entity
    {
        [Required, Range(minimum:5,maximum:20)]
        public string NameTeam { get; set; }
        [Required,Range(minimum:10, maximum:50)]
        public string ManagerName { get; set; }
        [Required,Range(minimum:10, maximum:50)]
        public string DelegadoName { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Required, Phone]
        public string PhoneNumberDelegado { get; set; }
        [Required]
        public string City { get; set; }
    }
}
