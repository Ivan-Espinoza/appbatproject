﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AppBatProjectApi.Model.Model
{
    public class Payment:Entity
    { 
        public int NumberCard { get; set; }

        public int DayCaducity { get; set; }

        public int MounthCaducity { get; set; }

        public int YearCaducity { get; set; }

        public int VerificationCard { get; set; }
        public string NameCard { get; set; }
    }
}
