﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppBatProjectApi.Model.Enums
{
    public enum TypeUser
    {
        Admin = 1,
        User = 2
    }
}
