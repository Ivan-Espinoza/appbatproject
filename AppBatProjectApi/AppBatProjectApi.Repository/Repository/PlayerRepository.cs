﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Repository.Repository
{
    public interface IPlayerRepository
    {
        IEnumerable<Player> GetAll();
        Task<Player> Get(int id);
        int Insert(Player player);
        int Delete(int id);
        Task<Player> Update(Player player);
    }

    public class PlayerRepository : IPlayerRepository
    {
        private readonly DataDbContext _dbContext = new DataDbContext();
        public int Delete(int id)
        {
            Player player = new Player();
            Task<Player> player_ = _dbContext.Player.FirstOrDefaultAsync(x => x.Id == id);
            player = player_.Result;
            _dbContext.Player.Remove(player);
            _dbContext.SaveChanges();
            return 1;
        }

        public Task<Player> Get(int id)
        {
            return _dbContext.Player.FirstOrDefaultAsync(x => x.Id == id);
        }

        public IEnumerable<Player> GetAll()
        {
            return _dbContext.Player;
        }

        public int Insert(Player player)
        {
            player.Id = 0;
            try
            {
                _dbContext.Add(player);
                _dbContext.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public Task<Player> Update(Player player)
        {
            try
            {
                _dbContext.Entry(player).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return _dbContext.Player.FirstOrDefaultAsync(x => x.Id == player.Id);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
