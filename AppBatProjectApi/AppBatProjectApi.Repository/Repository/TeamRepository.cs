﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Repository.Repository
{
    public interface ITeamRepository
    {
        IEnumerable<Team> GetAll();
        Task<Team> GetTeam(int id);
        int Delete(int id);
        int Insert(Team team);
        Task<Team> Update(Team team);
    }
    public class TeamRepository : ITeamRepository
    {
        private readonly DataDbContext _db = new DataDbContext();
        public int Delete(int id)
        {
            Team team = new Team();
            Task<Team> teamT = _db.Team.FirstOrDefaultAsync(x => x.Id == id);
            team = teamT.Result;
            _db.Team.Remove(team);
            _db.SaveChanges();
            return 1;
        }

        public IEnumerable<Team> GetAll()
        {
            return _db.Team;
        }

        public Task<Team> GetTeam(int id)
        {
            return _db.Team.FirstOrDefaultAsync(x => x.Id == id);
        }

        public int Insert(Team team)
        {
            team.Id = 0;
            try
            {
                _db.Add(team);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public Task<Team> Update(Team team)
        {
            try
            {
                _db.Entry(team).State = EntityState.Modified;
                _db.SaveChanges();
                return _db.Team.FirstOrDefaultAsync(x => x.Id == team.Id);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
