﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Repository.Repository
{
    public interface INoteRepository
    {
        IEnumerable<Note> GetAllNote();
        Task<Note> GetNote(int id);
        int Insert(Note note);
        Task<Note> UpdateNote(Note note);
        int DeleteNote(int id);
    }
    public class NoteRepository : INoteRepository
    {
        private readonly DataDbContext _db = new DataDbContext();

        public int DeleteNote(int id)
        {
            Note note = new Note();
            Task<Note> noteTask = _db.Note.FirstOrDefaultAsync(x => x.Id == id);
            note = noteTask.Result;
            _db.Note.Remove(note);
            _db.SaveChanges();
            return 1;
        }

        public IEnumerable<Note> GetAllNote()
        {
            return _db.Note;
        }

        public Task<Note> GetNote(int id)
        {
            return _db.Note.FirstOrDefaultAsync(x => x.Id == id);
        }

        public int Insert(Note note)
        {
            note.Id = 0;
            try
            {
                _db.Add(note);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
                
            }

        }

        public Task<Note> UpdateNote(Note note)
        {
            try
            {
                _db.Entry(note).State = EntityState.Modified;
                _db.SaveChanges();
                return _db.Note.FirstOrDefaultAsync(x => x.Id == note.Id);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
