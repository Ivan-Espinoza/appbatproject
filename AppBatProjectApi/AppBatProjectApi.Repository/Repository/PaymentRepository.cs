﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Repository.Repository
{

    public interface IPaymentRepository
    {
        Task<IEnumerable<Payment>> GetAllPayment();
        Task<Payment> GetIdPayment(int id);
        Task<Payment>InsertPayment(Payment payment);
        Task<Payment> UpdatePayment(Payment payment, int id);
        Task<Payment> DeletePayment(int id);
    }
    public class PaymentRepository : IPaymentRepository
    {
        private readonly DataDbContext _context;

        public PaymentRepository(DataDbContext context)
        {
            _context = context;
        }
        public async Task<Payment> DeletePayment(int id)
        {
            var payment = await _context.Payment.FindAsync(id);
            _context.Payment.Remove(payment);
            await _context.SaveChangesAsync();
            return payment;
        }

        public async Task<IEnumerable<Payment>> GetAllPayment()
        {
            var payment = await _context.Payment.ToArrayAsync();
            return (payment);
        }

        public async Task<Payment> GetIdPayment(int id)
        {
            var payment = await _context.Payment.FindAsync(id);
            return (payment);
        }

        public async Task<Payment> InsertPayment(Payment payment)
        {
            _context.Payment.Add(payment);
            await _context.SaveChangesAsync();
            return payment;
        }

        public async Task<Payment> UpdatePayment(Payment payment, int id)
        {
            _context.Entry(payment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return payment;
        }
    }
}
