﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Repository.Repository
{
    public interface IPitcherEstadisticasRepository
    {
        Task<PitcherEstadisticas> Get(int id);
        int Insert(PitcherEstadisticas pitcherEstadisticas);
        Task<PitcherEstadisticas> Update(PitcherEstadisticas pitcherEstadisticas);
    }
    public class PitcherEstadisticasRepository : IPitcherEstadisticasRepository
    {
        private readonly DataDbContext db = new DataDbContext();

        public Task<PitcherEstadisticas> Get(int id)
        {
            return db.PitcherEstadisticas.FirstOrDefaultAsync(x => x.Id == id);
        }

        public int Insert(PitcherEstadisticas pitcherEstadisticas)
        {
            pitcherEstadisticas.Id = 0;
            try
            {
                db.Add(pitcherEstadisticas);
                db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public Task<PitcherEstadisticas> Update(PitcherEstadisticas pitcherEstadisticas)
        {
            try
            {
                db.Entry(pitcherEstadisticas).State = EntityState.Modified;
                db.SaveChanges();
                return db.PitcherEstadisticas.FirstOrDefaultAsync(x => x.Id == pitcherEstadisticas.Id);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
