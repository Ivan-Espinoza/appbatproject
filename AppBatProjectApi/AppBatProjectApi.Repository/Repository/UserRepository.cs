﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Repository.Repository
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
        Task<User> Get(int Id);
        int Insert(User user);
        int Delete(int id);
        Task<User> Update(User user);
        User userByEmailAndPassword(Login login);

    }
    public class UserRepository : IUserRepository
    {
        private readonly DataDbContext _db = new DataDbContext();
        public int Delete(int id)
        {
            User user = new User();
            Task<User> userTask = _db.User.FirstOrDefaultAsync(x => x.Id == id);
            user = userTask.Result;
            _db.User.Remove(user);
            _db.SaveChanges();
            return 1;
        }

        public Task<User> Get(int Id)
        {
            return _db.User.FirstOrDefaultAsync(x => x.Id == Id);
        }

        public IEnumerable<User> GetAll()
        {
            return _db.User;
        }

        public int Insert(User user)
        {
            user.Id = 0;
            try
            {
                _db.Add(user);
                _db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public Task<User> Update(User user)
        {
            try
            {
                _db.Entry(user).State = EntityState.Modified;
                _db.SaveChanges();
                return _db.User.FirstOrDefaultAsync(x => x.Id == user.Id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public User userByEmailAndPassword(Login login)
        {
            return GetAll().FirstOrDefault(x => x.Email.ToLower() == login.Email.ToLower()
            && x.Password.ToLower() == login.Password.ToLower());
        }
    }
}
