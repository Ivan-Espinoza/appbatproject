﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AppBatProjectApi.Repository.Migrations
{
    public partial class AppBatDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PitcherEstadisticas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NamePlayer = table.Column<string>(nullable: true),
                    Team = table.Column<string>(nullable: true),
                    NumberGame = table.Column<int>(nullable: false),
                    GameWiner = table.Column<int>(nullable: false),
                    GameLoser = table.Column<int>(nullable: false),
                    Era = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PitcherEstadisticas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PitcherEstadisticas");
        }
    }
}
