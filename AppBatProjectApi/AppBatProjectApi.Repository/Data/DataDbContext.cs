﻿using AppBatProjectApi.Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppBatProjectApi.Repository.Data
{
    public class DataDbContext : DbContext
    {
        public DataDbContext() { }

        public DataDbContext(DbContextOptions options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //@"Server=DESKTOP-80RA4GE;Database=Veterinary;Uid=saj;Pwd=sesamo23;"
                optionsBuilder.UseSqlServer(@"Server=.;Database=AppBat;Uid=sa;Pwd=uts;");
            }

        }

        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Note> Note { get; set; }
        public virtual DbSet<Player> Player { get; set; }
        public virtual DbSet<Team> Team { get; set; }
        public virtual DbSet<PitcherEstadisticas> PitcherEstadisticas { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
    }
}