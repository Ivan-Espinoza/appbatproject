﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace AppBatProjectApi.Service.Service
{
    public interface ISendMailService
    {
        bool SendMessage(string Mail, string subjet, string Message);
    }
    public class SendMailService : ISendMailService
    {
        private SmtpClient cliente;

        public bool SendMessage(string Mail, string subjet, string Message)
        {
            try
            {
                cliente = new SmtpClient("smtp.gmail.com", 587)
                {
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential("", "")
                };
                cliente.Send("", Mail, subjet, Message);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
