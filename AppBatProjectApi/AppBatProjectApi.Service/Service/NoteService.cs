﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Service.Service
{
    public interface INoteService
    {
        IEnumerable<Note> GetAllNote();
        Task<Note> GetNote(int id);
        void InsertNote(Note note);
        Task<Note> UpdateNote(Note note);
        int DeleteNote(int id);
    }
    public class NoteService : INoteService
    {
        private readonly INoteRepository _noteRepository;
        public NoteService(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }

        public void InsertNote(Note note)
        {
            _noteRepository.Insert(note);
        }

        public int DeleteNote(int id)
        {
           return _noteRepository.DeleteNote(id);
        }

        public IEnumerable<Note> GetAllNote()
        {
            return _noteRepository.GetAllNote();
        }

        public Task<Note> GetNote(int id)
        {
           return _noteRepository.GetNote(id);
        }

        public Task<Note> UpdateNote(Note note)
        {
            try
            {
                return _noteRepository.UpdateNote(note);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
