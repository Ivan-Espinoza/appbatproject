﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Service.Service
{
    public interface ITeamService
    {
        IEnumerable<Team> GetAll();
        Task<Team> GetTeam(int id);
        int Delete(int id);
        void Insert(Team team);
        Task<Team> Update(Team team);
    }
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _teamRepository;
        public TeamService(ITeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }
        public int Delete(int id)
        {
            return _teamRepository.Delete(id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _teamRepository.GetAll();
        }

        public Task<Team> GetTeam(int id)
        {
            return _teamRepository.GetTeam(id);
        }

        public void Insert(Team team)
        {
            _teamRepository.Insert(team);
        }

        public Task<Team> Update(Team team)
        {
            try
            {
                return _teamRepository.Update(team);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
