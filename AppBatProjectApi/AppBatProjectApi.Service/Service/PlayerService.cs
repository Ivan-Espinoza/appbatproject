﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Service.Service
{
    public interface IPlayerService
    {
        IEnumerable<Player> GetAll();
        Task<Player> Get(int id);
        void Insert(Player player);
        int Delete(int id);
        Task<Player> Update(Player player);
    }
    public class PlayerService : IPlayerService
    {
        private readonly IPlayerRepository _playerRepository;

        public PlayerService(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public int Delete(int id)
        {
           return _playerRepository.Delete(id);
        }

        public Task<Player> Get(int id)
        {
            return _playerRepository.Get(id);
        }

        public IEnumerable<Player> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Insert(Player player)
        {
            _playerRepository.Insert(player);
        }

        public Task<Player> Update(Player player)
        {
            try
            {
                return _playerRepository.Update(player);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
