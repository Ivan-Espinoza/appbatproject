﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Service.Service
{

    public interface IPaymentService
    {
        Task<IEnumerable<Payment>> GetAllPayment();
        Task<Payment> GetIdPayment(int Id);
        Task<Payment> InsertPayment(Payment payment);
        Task<Payment> UpdatePayment(Payment payment, int id);
        Task<Payment> DeletePayment(int id);
    }


    public class PaymentService : IPaymentService
    {
        //Inyeccion de dependencias (usar metodos de otra clase o interfaz)
        private readonly IPaymentRepository _paymentRepository;
        public PaymentService(IPaymentRepository paymentRepository)
        {
            this._paymentRepository = paymentRepository;
        }
        //Inyeccion de dependencias (usar metodos de otra clase o interfaz)

        public async Task<Payment>DeletePayment(int id)
        {
            var payment = await _paymentRepository.DeletePayment(id);
            return payment;
        }

        public async Task<IEnumerable<Payment>> GetAllPayment()
        {
            var payment = await _paymentRepository.GetAllPayment();
            return (payment);
        }

        public async Task<Payment> GetIdPayment(int Id)
        {
            var payment = await _paymentRepository.GetIdPayment(Id);
            return (payment);
        }

        public async Task<Payment>InsertPayment(Payment payment)
        {
            await _paymentRepository.InsertPayment(payment);
            return payment;
        }

        public async Task<Payment> UpdatePayment(Payment payment,int id)
        {
            await _paymentRepository.UpdatePayment(payment, id);
            return payment;
        }
    }
}
