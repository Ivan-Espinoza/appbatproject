﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Service.Service
{
    public interface IUserService
    {
        IEnumerable<User> GetUsers();
        Task<User> Get(int Id);
        void AddUser(User user);
        Task<User> UpdateUser(User user);
        int Delete(int Id);
        User Login(Login login);
    }

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
     
        public void AddUser(User user)
        {
            _userRepository.Insert(user);
        }

        public int Delete(int Id)
        {
            return _userRepository.Delete(Id);
        }

        public Task<User> Get(int Id)
        {
            return _userRepository.Get(Id);
        }

        public IEnumerable<User> GetUsers()
        {
            return _userRepository.GetAll();
        }

        public User Login(Login login)
        {
            return _userRepository.userByEmailAndPassword(login);  
        }

        public Task<User> UpdateUser(User user)
        {
            try
            {
                return _userRepository.Update(user);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
