﻿using AppBatProjectApi.Model.Model;
using AppBatProjectApi.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppBatProjectApi.Service.Service
{
    public interface IPitcherEstadisticasService
    {
        Task<PitcherEstadisticas> Get(int id);
        void Insert(PitcherEstadisticas pitcherEstadisticas);
        Task<PitcherEstadisticas> Update(PitcherEstadisticas pitcherEstadisticas);

    }
    public class PitcherEstadisticasService: IPitcherEstadisticasService
    {
        private readonly IPitcherEstadisticasRepository _pitcherEstadisticasRepository;

        public PitcherEstadisticasService(IPitcherEstadisticasRepository pitcherEstadisticasRepository)
        {
            _pitcherEstadisticasRepository = pitcherEstadisticasRepository;
        }

        public Task<PitcherEstadisticas> Get(int id)
        {
            return _pitcherEstadisticasRepository.Get(id);
        }

        public void Insert(PitcherEstadisticas pitcherEstadisticas)
        {
            _pitcherEstadisticasRepository.Insert(pitcherEstadisticas);
        }

        public Task<PitcherEstadisticas> Update(PitcherEstadisticas pitcherEstadisticas)
        {
            return _pitcherEstadisticasRepository.Update(pitcherEstadisticas);
        }
    }
}
